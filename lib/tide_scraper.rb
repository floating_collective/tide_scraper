require 'tide_scraper/version'
require 'tide_scraper/scraper'
require 'nokogiri'
require 'httpclient'
require 'date'

module TideScraper
  # Return the next prediction for a sorted list of tides
  # (produced by get_prediction - sorted by default)
  def self.get_next_tide(prediction)
    prediction.each do |tide|
      if tide[:time] > Time.now
        return tide
      end
    end
    # Should have found a tide by now. If not, empty set
    return []
  end

  # Given a sorted list of tides,
  # only return tides that are in the future
  def self.get_upcoming_tides(prediction)
    future_tides = []
    prediction.each do |tide|
      if tide[:time] > Time.now
        future_tides.push tide
      end
    end
    return future_tides
  end
end
