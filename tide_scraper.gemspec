# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tide_scraper/version'

Gem::Specification.new do |spec|
  spec.name          = "tide_scraper"
  spec.version       = TideScraper::VERSION
  spec.authors       = ["Martin Pugh"]
  spec.email         = ["pugh@s3kr.it"]

  spec.summary       = %q{Tool for scraping tide data from UKHO EasyTide}
  spec.homepage      = "http://github.com/s3krit/tide_scraper"
  spec.license       = "GPL-3.0"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "nokogiri", "~> 1.6"
  spec.add_dependency "httpclient", "~> 2.7"

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "pry", "~> 0.10"
end
