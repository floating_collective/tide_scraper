# TideScraper

TideScraper simple gem for scraping tidal predictons from the UKHO's EasyTide system.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tide_scraper'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tide_scraper

## Usage

Assuming id = a UKHO port ID as a string:

```
require 'tide_scraper'
scraper = TideScraper::Scraper.new
puts scraper.get_prediction(id)
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/s3krit/tide_scraper.


## License

The gem is available as open source under the terms of the [GPL-3.0](http://opensource.org/licenses/GPL-3.0).

